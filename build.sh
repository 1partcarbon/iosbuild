set -e

xcodebuild -project Testy.xcodeproj -scheme Testy -configuration Debug -sdk iphonesimulator clean build | xcpretty --no-utf && exit ${PIPESTATUS[0]}
