set -e
xcodebuild test -scheme TestyTests -destination 'platform=iOS Simulator,name=iPhone Retina (4-inch 64-bit),OS=8.1' | xcpretty -t --no-utf && exit ${PIPESTATUS[0]}
